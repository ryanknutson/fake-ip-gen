# fake IP generator
this C program will generate a "fake" IP address (an address in private space)
for use in some harmless online trolling

## installing
simply run `make`

## running
simply run `./fakeip`

## options
you can choose the exact block of private IPs you want to generate with
`./fakeip BLOCK TIMESTORUN`

BLOCK can be any of the below:
- 0 or 10 (10.0.0.0 – 10.255.255.255)
- 1 or 172 (172.16.0.0 – 172.31.255.255)
- 2 or 192 (192.168.0.0 – 192.168.255.255)
