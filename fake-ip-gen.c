/* "fake" private IP address generator */
/* Copyright (C) 2023 Ryan D. Knutson */

/* This program is free software; you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation; either version 2 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License along */
/* with this program; if not, write to the Free Software Foundation, Inc., */
/* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

#ifndef __unix__
  #include <time.h>
#endif

// fill the passed in array with 'size' random values
int *genRandArray(int* arr, short int size) {
  // unix systems (/dev/urandom)
  #ifdef __unix__
    int random_fd = open("/dev/urandom", O_RDONLY);
  
    // set up a buffer
    unsigned char buffer[size * sizeof(int)];
  
    // read /dev/urandom into the buffer
    read(random_fd, buffer, sizeof(buffer));
    close(random_fd);
  
    // Convert the bytes into integer values
    for (short int i = 0; i < size; ++i) {
      arr[i] = *((int*)(buffer + i * sizeof(int)));
    }
  #else
    for (short int i = 0; i < size; ++i) {
      arr[i] = rand();
    }
  #endif
  return 0;
}

// constrain random number to range (must be positive)
int constrain(int in, short int min, short int max) {
  return (abs(in) % (max - min + 1)) + min;
}

// generate a random ip in ipBlock
int randIP(int ipBlock) {
  // set array size based on input block
  // if random (default) we won't know until ipblock is created
  // set it to 4 because that is the max number of values we need
  short int arrSize = 4;
  switch (ipBlock) {
    case 0: arrSize = 3; break;
    case 1: arrSize = 3; break;
    case 2: arrSize = 2; break;
  }

  // create a dynamic array with size arrSize
  // this will contain our random values
  int* randVals = (int*)malloc(arrSize * sizeof(int));

  // fill the array with random values
  genRandArray(randVals, arrSize);

  // if ipBlock is -1, generate one
  if (ipBlock == -1) ipBlock = constrain(randVals[3], 0, 2);


  // generate ip using constraints
  // NOTE this section works backwards
  
  // create an array to store ip octets
  short int ip[4] = {0};

  // since all private ip spaces can end with values up to 255, generate those
  ip[3] = constrain(randVals[0], 0, 255);
  ip[2] = constrain(randVals[1], 0, 255);

  // generate the rest of the ip values
  switch (ipBlock) {
    case 0: // 10.*
      ip [1] = constrain(randVals[2], 0, 255);
      ip[0] = 10;
      break;
    case 1: // 172.*
      ip[1] = constrain(randVals[2], 16, 31);
      ip[0] = 172;
      break;
    case 2: // 192.168.*
      ip[1] = 168;
      ip[0] = 192;
  }

  free(randVals);

  // print out the ip to stdout
  printf("%d.%d.%d.%d\n", ip[0],ip[1],ip[2],ip[3]);

  return 0;
}

short int setIPBlock(short int input) {
  switch (input) {
    case 10:;
    case 0: return 0;
    case 172:;
    case 1: return 1;
    case 192:;
    case 2: return 2;
  }
  return -1;
}

int argvToInt(char *argv[], short int pos) {
  char *endptr;
  int res = strtol(argv[pos], &endptr, 10);

  // Exit with an error code if the conversion fails
  if (*endptr != '\0') {
    fprintf(stderr, "Invalid input: %s\n", argv[pos]);
    exit(1);
  }

  return res;
}

int main(int argc, char *argv[]) {
  // if system is not a unix system, use srand instead of /dev/urandom
  #ifndef __unix__
    srand(time(NULL));
  #endif

  int timesToRun = 1;
  short int ipBlock = -1;

  // handle command line arguments
  // ./fakeip ipBlock timesToRun
  switch (argc-1) {
    case 2: // ipBlock and times to run
      timesToRun = argvToInt(argv, 2);
    case 1: // ipBlock
      ipBlock = argvToInt(argv, 1);
  }

  // handle ip block argument
  switch (ipBlock) {
    case 10:;
    case 0: ipBlock = 0; break;
    case 172:;
    case 1: ipBlock = 1; break;
    case 192:;
    case 2: ipBlock = 2; break;
    default: printf("invalid ip block entered! picking randomly...\n");
    case -1:;
  }

  // handle timesToRun
  if (timesToRun < 1) {
    printf("invalid number of times to run entered! running once...\n");
    timesToRun = 1;
  }

  // generate random ips
  for (int i = 0; i < timesToRun; ++i) {
    randIP(ipBlock);
  }

  return 0;
}
